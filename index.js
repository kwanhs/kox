const koa = require('koa');
const app = new koa();

app
    .use(async ctx => {
        ctx.body = 'Hello there.\n';
        ctx.body += `You are ${ctx.request.ip}.\n`
    })
    .listen(8080, '0.0.0.0');